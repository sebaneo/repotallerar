var clientesObtenidos;

function gestionClientes() {
	var url="https://services.odata.org/V4/Northwind/Northwind.svc/Customers";
	var request = new XMLHttpRequest();

	request.onreadystatechange=function() {
		//con 4 es que termino de validar la peticion, que esta respondido
		if(this.readyState == 4 && this.status == 200) {
			clientesObtenidos=request.responseText;	
			procesarClientes();
		}
	}

	request.open("GET", url, true);
	request.send();
}

function procesarClientes() {
	var rutaBandera="https://www.countries-ofthe-world.com/flags-normal/flag-of-"
	var JSONClientes = JSON.parse(clientesObtenidos);
	var divTabla=divTablaClientes;
	var tabla = document.createElement("table");
	var tbody = document.createElement("tbody");

	//que sea una tabla de bootstrap a rayas

	tabla.classList.add("table");
	tabla.classList.add("table-striped");

	for (var i=0; i<JSONClientes.value.length; i++) {
		var nuevaFila=document.createElement("tr");
		var columnaNombre=document.createElement("td");
		columnaNombre.innerText = JSONClientes.value[i].ContactName;
		var columnaCompany=document.createElement("td");
		columnaCompany.innerText = JSONClientes.value[i].CompanyName;
		var columnaPais=document.createElement("td");
		var imgBandera = document.createElement("img");
		imgBandera.classList.add("flag");
		if(JSONClientes.value[i].Country == "UK"){
			imgBandera.src=rutaBandera + "United-Kingdom.png";
		} else {
			imgBandera.src=rutaBandera + JSONClientes.value[i].Country + ".png";
		}
		//columnaPais.innerText = JSONClientes.value[i].Country;
		columnaPais.appendChild(imgBandera);
		nuevaFila.appendChild(columnaNombre);
		nuevaFila.appendChild(columnaCompany);
		nuevaFila.appendChild(columnaPais);
		tbody.appendChild(nuevaFila);
	}
	tabla.appendChild(tbody);
	divTabla.appendChild(tabla);
}
